# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# isx25633105 ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys,socket, argparse, os, signal, time, select
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""Server que envia la data un a un""",epilog="(8)(8)")
parser.add_argument("-p",type=int,help="Introdueix el port el per defecte es 50001",dest="port",default=50001)
args=parser.parse_args()
HOST = ''
PORT = args.port 
pid=os.fork()
if pid !=0:
  print("Engegat el server ps:", pid)
  sys.exit(0)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
conns=[s]
while True:
    actius,x,y = select.select(conns,[],[])
    for actual in actius:
        if actual == s:
            conn, addr = s.accept()
            print('Connected by', addr)
            conns.append(conn)
        else:
            while True:
                data = actual.recv(1024)
                if not data:
                    sys.stdout.write("Client finalitzat: %s \n" % (actual))
                    actual.close()
                    conns.remove(actual)
                    break
                else:
                    pipeData = Popen(data,shell=True,stdout=PIPE)
                    for line in pipeData.stdout:
                        conn.send(line)
                    conn.send(b'\x04')
s.close()
sys.exit(0)
