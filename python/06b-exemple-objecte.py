# /usr/bin/python3
#-*- coding: utf-8-*-
#
# -------------------------------------
# isx25633105 ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
# Exemple de programació Objectes POO
# -------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self, userLine):	
      "Constructor objectes UnixUser"
      userField=userLine.split(":")
      self.login=userField[0]
      self.passwd=userField[1]
      self.uid=int(userField[2])
      self.gid=int(userField[3])
      self.gecos=userField[4]
      self.home=userField[5]
      self.shell=userField[6]
  def show(self):
    "Mostra les dades de l'usuari"
    print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gecos: {self.gecos} home: {self.home} shell: {self.shell}")

  def __str__(self):
      return "%s %s %d %d %s %s %s " % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
print("Programa")
user1=UnixUser("Bob:x:1000:100::/home/pere:/bin/bash")
print(user1)
user1.show()
exit(0)

