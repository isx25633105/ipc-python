# /usr/bin/python
#-*- coding: utf-8-*-
#
# list-users [-f file]
# 10 lines, file o stdin
# -------------------------------------
# isx25633105 ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, argparse
parser = argparse.ArgumentParser(description="""Llistar els usuaris de file o stdin (format /etc/passwd""", epilog="Not to copy please")
parser.add_argument("-f","--fit",type=str, help="user file or stdin (/etc/passwd style)", metavar="file", default="/dev/stdin",dest="fitxer")
args=parser.parse_args()
# -------------------------------------------------------
class UnixUser():
  """Classe UnixUser: base  de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser del exercici 6b"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6][:-1]
  def show(self):
    "Mostra les dades de l'usuari del exercici 6b"
    print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gecos: {self.gecos} home: {self.home} shell: {self.shell}")  
  def sumaun(self):
    "funcio que suma el uid del exercici 6"
    self.uid+=1
  def __str__(self):
    "functió to_string del exercici 6b"
    return "%s %d %d" % (self.login, self.uid, self.gid)
# -------------------------------------------------------
fileIn=open(args.fitxer,"r")
userList=[]
for line in fileIn:
  user=UnixUser(line)
  userList.append(user)
fileIn.close()
for user in userList:
 print(user)
exit(0)

