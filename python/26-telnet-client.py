# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# isx25633105 ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, os, argparse, socket
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""Servei telnet """,epilog="(8)(8)")
parser.add_argument("-p",type=int,help="Introdueix el port el per defecte es 50001",dest="port",default=50001)
parser.add_argument("server",type=str,help="Introdueix el server")
args=parser.parse_args()
HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:
  cmd = input("telnet>")
  if cmd == 'exit':
    s.close()
    break
  else:
    cmd = cmd.encode()
    s.send(cmd)
  while True:
    data = s.recv(1024)
    print (str(data))		
    if b'\x04' in data: 
      break
print("Exit")
sys.exit(0)
