# /usr/bin/python3
#-*- coding: utf-8-*-
#
# signal-exemple.py
# -------------------------------------
# isx25633105 ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, os, signal, argparse 
parser = argparse.ArgumentParser(description="Gestionar l'alarma")
parser.add_argument("segons", type=int, help="segons")
args=parser.parse_args()
global upp
global down
upp = 0
down = 0

def messegons(signum, frame):
    global upp
    upp+=1
    actual=signal.alarm(0)
    suma=int(actual+60)
    signal.alarm(suma)
    print("Signal handler called with signal:", signum)
def menossegons(sginum, frame):
    global down
    down = 0
    actual=signal.alarm(0)
    resta=int(actual-60)
    if resta > 0:
        signal.alarm(resta)
        print("Signal handler called with signal:", signum)
    else:
        sys.exit(0)
def reinicia(sginum, frame):
    signal.alarm(args.segons)
    print(signal.alarm(0))
    print("Signal handler called with signal:", signum)
def segonsfalten(sginum, frame):
    print(signal.alarm(0))
    print("Signal handler called with signal:", signum)
def mysigalarm(signum,frame):
  global upp, down
  print("Signal handler called with signal:", signum)
  print("Finalitzant.... upp:%d  down:%d" % (upp, down))
  sys.exit(1)
signal.signal(signal.SIGUSR1,messegons)   
signal.signal(signal.SIGUSR2,menossegons)
signal.signal(signal.SIGHUP,reinicia)
signal.signal(signal.SIGINT,signal.SIG_IGN)
signal.signal(signal.SIGTERM,segonsfalten)
signal.alarm(args.segons)
print(os.getpid())
while True:
  pass
signal.alarm()
sys.exit(0)

