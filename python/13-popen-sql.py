# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql.py
# -------------------------------------
# isx25633105 ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
import argparse
parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
parser.add_argument("cons", type=str, help='Sentència SQL a executar')
args = parser.parse_args()
# -------------------------------------------------------
command = [f"psql -qtA -F',' -h 10.200.243.205 -U postgres training -c \"{args.cons}\""]
pipeData = Popen(command,shell=True,stdout=PIPE)
for line in pipeData.stdout:
    print(line.decode("utf-8"), end="")
exit(0)
