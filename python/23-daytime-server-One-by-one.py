# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# isx25633105 ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys,socket, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""Server que envia la data un a un""",epilog="(8)(8)")
parser.add_argument("-p",type=int,help="Introdueix el port el per defecte es 50001",dest="port",default=50001)
args=parser.parse_args()
HOST = ''
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
    conn, addr = s.accept()
    #print("Conn", type(conn), conn)
    print("Connected by", addr)
    command = ["date"]
    pipeData = Popen(command,stdout=PIPE)
    for line in pipeData.stdout:
        conn.send(line)
    conn.close()
