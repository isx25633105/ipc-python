# /usr/bin/python
#-*- coding: utf-8-*-
import sys
from subprocess import Popen, PIPE
import argparse
parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
parser.add_argument('cons', help='Sentència SQL a executar',metavar='sentènciaSQL')
args = parser.parse_args()

cmd = "psql -qtA -F',' -h 172.17.0.2 -U postgres  training"
#py2# pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData = Popen(cmd, shell = True, bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write("{args.cons};\n\q\n")

for line in pipeData.stdout:
    print(line, end="")
sys.exit(0)

