# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# isx25633105 ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys,socket, argparse, os, signal, time
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""Server que envia la data un a un""",epilog="(8)(8)")
parser.add_argument("-p",type=int,help="Introdueix el port el per defecte es 50001",dest="port",default=50001)
args=parser.parse_args()
HOST = ''
PORT = args.port 
pid=os.fork()
if pid !=0:
  print("Engegat el server ps:", pid)
  sys.exit(0)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    while True:
      data = conn.recv(1024)
      pipeData = Popen(data,shell=True,stdout=PIPE)
      if not data:
        conn.close()
        break
      print(str(data))
      for line in pipeData.stdout:
        conn.send(line)
      conn.send(b'\x04')

