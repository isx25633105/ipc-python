# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# isx25633105 ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys,socket, argparse, os, signal, time
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""Server que envia la data un a un""",epilog="(8)(8)")
parser.add_argument("-p",type=int,help="Introdueix el port el per defecte es 50001",dest="port",default=50001)
args=parser.parse_args()
llistaPeers=[]
HOST = ''
PORT = args.port
def mysigusr1(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers)
  sys.exit(0)
  
def mysigusr2(signum,frame):
  print("Signal handler called with signal:", signum)
  print(len(llistaPeers))
  sys.exit(0)

def mysigterm(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers, len(llistaPeers))
  sys.exit(0)
  
pid=os.fork()
if pid !=0:
  print("Engegat el server ps:", pid)
  sys.exit(0)
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    llistaPeers.append(addr)
    fileName="/tmp/%s-%s-%s.log" % (addr[0],addr[1],time.strftime("%Y%m%d-%H%M%S"))
    fileLog=open(fileName,"w")
    while True:
        data = conn.recv(1024)
        if not data: break
        fileLog.write(str(data))
    conn.close()
    fileLog.close()

